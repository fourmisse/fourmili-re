package model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * Classe repr�sentant le terrain de la simulation.
 * 
 * @author Gurvan & Samuel
 *
 */
public class Terrain {

	/**
	 * Liste des fourmili�res pr�sentes dans la simulation
	 */
	private List<Fourmiliere> listeFourmiliere;

	/**
	 * Liste des ph�romones pr�sentes sur le terrain
	 */
	private Map<Coordonnee, Pheromone> listePheromone;

	/**
	 * Liste des proies pr�sentes sur le terrain
	 */
	private List<Proie> listeProies;

	/**
	 * Distance (en m�tre) entre deux blocs
	 */
	public static final int METRE_BLOCS = 5;

	/**
	 * Constructeur du terrain
	 */
	public Terrain() {
		this.listeFourmiliere = new ArrayList<Fourmiliere>();
		this.listePheromone = new HashMap<Coordonnee, Pheromone>();
		this.listeProies = new ArrayList<Proie>();
	}

	/**
	 * R�cup�ration de la liste des fourmili�res existantes
	 * 
	 * @return La liste des fourmili�res de la simulation
	 */
	public List<Fourmiliere> getListeFourmiliere() {
		return listeFourmiliere;
	}

	/**
	 * Permet l'ajout de ph�romone sur une case pr�cise du terrain
	 * 
	 * @param x
	 * @param y
	 */
	public void addPheromoneOuvriere(int x, int y) {
		this.listePheromone.put(new Coordonnee(x, y), new PheromoneObservation(Simulateur.getHeures()));
	}

	/**
	 * Permet la v�rification de la validit� des ph�romones
	 * 
	 * @return La liste des coordonn�es contenant des ph�romones invalides
	 */
	public List<Coordonnee> verifierPheromone() {
		Iterator<Coordonnee> ite = listePheromone.keySet().iterator();
		List<Coordonnee> coordonneeInvalides = new ArrayList<>();

		while (ite.hasNext()) {
			Coordonnee coordonnee = ite.next();
			if (!this.listePheromone.get(coordonnee).isValid()) {
				coordonneeInvalides.add(coordonnee);
				ite.remove();
			}
		}

		return coordonneeInvalides;
	}

	/**
	 * R�cup�ration d'une Map contenant des coordonn�es en clef associ�es � la
	 * pr�sence d'une ph�romone
	 * 
	 * @return Une map contenant les coordonn�es o� sont situ�es des ph�romones
	 */
	public Map<Coordonnee, Pheromone> getListePheromone() {
		return this.listePheromone;
	}

	/**
	 * R�cup�ration de la liste des proies pr�sentes sur le terrain
	 * 
	 * @return La liste des proies
	 */
	public List<Proie> getListeProies() {
		return this.listeProies;
	}
}
