package model;

import model.etat.Adulte;
import model.etat.EtatFourmi;
import model.etat.Oeuf;
import model.role.Role;

/**
 * La classe repr�sentant les fourmis de la simulation. �tend la classe
 * �treVivant.
 * 
 * @author Gurvan & Samuel
 *
 */
public class Fourmi extends EtreVivant {

	/**
	 * Distance maximale � laquelle une fourmi peut se d�placer en dehors de la
	 * fourmili�re
	 */
	private static final int distanceMax = 200;

	/**
	 * �tat actuel de la fourmi
	 */
	private EtatFourmi etat;

	/**
	 * Date du dernier changement d'�tat de la fourmi
	 */
	private int dateChangementEtat;

	/**
	 * Coordonn�es de la fourmi sur la carte
	 */
	private Coordonnee coordonnee;

	/**
	 * Constructeur de la fourmi. On y fourni les coordonn�es de d�part
	 * 
	 * @param x
	 * @param y
	 */
	public Fourmi(int x, int y) {
		// Initialisation des coordonn�es de d�part sur la carte
		this.coordonnee = new Coordonnee(x, y);

		// Initialisation d'un poids entre 1.5 et 2 mg
		this.setPoids((Math.random() / 2) + 1.5);

		// Initialisation du premier �tat de la fourmi
		this.etat = new Oeuf(this);
	}

	/**
	 * R�cup�ration du r�le de la fourmi
	 * 
	 * @return Le r�le de la fourmi
	 */
	public Role getRole() {
		Adulte etat = (Adulte) (this.getEtat());
		return etat.getRole();
	}

	/**
	 * R�cup�ration de l'�tat de la fourmi
	 * 
	 * @return L'�tat de la fourmi
	 */
	public EtatFourmi getEtat() {
		return etat;
	}

	/**
	 * Mise � jour de l'�tat de la fourmi
	 * 
	 * @param etat �tat � appliquer
	 */
	public void setEtat(EtatFourmi etat) {
		this.etat = etat;
	}

	/**
	 * Cette methode permet de faire evoluer la fourmi du stade de l'oeuf au stade
	 * de fourmi
	 */
	public void step(Fourmiliere fourmiliere, Simulateur simulateur) {
		// Passage au prochain �tat de la fourmi si la date est ariv�e
		if (this.dateChangementEtat > this.getEtat().getDuree()) {
			this.getEtat().next(this, fourmiliere, simulateur.getTerrain());
		}

		// Passage � la prochaine �tape pour chaque fourmi
		this.etat.step(this, fourmiliere, simulateur.getTerrain());

		// Incr�mentation de la date du dernier changement d'�tat
		this.dateChangementEtat++;
	}

	/**
	 * R�cup�ration de la coordonn�e en abscisse de la fourmi
	 * 
	 * @return La coordonn�e en abscisse de la fourmi
	 */
	public int getX() {
		return this.getCoordonnee().getX();
	}

	/**
	 * Mise � jour de la coordonn�e en abscisse de la fourmi
	 * 
	 * @param x La nouvelle coordonn�e en abscisse
	 */
	public void setX(int x) {
		this.getCoordonnee().setX(x);
	}

	/**
	 * R�cup�ration de la coordonn�e en ordonn�e de la fourmi
	 * 
	 * @return La coordonn�e en ordonn�e de la fourmi
	 */
	public int getY() {
		return this.getCoordonnee().getY();
	}

	/**
	 * Mise � jour de la coordonn�e en ordonn�e de la fourmi
	 * 
	 * @param y La nouvelle coordonn�e en ordonn�e
	 */
	public void setY(int y) {
		this.getCoordonnee().setY(y);
	}

	/**
	 * R�cup�ration des coordonn�es de la fourmi
	 * 
	 * @return Les coordonn�es de la fourmi
	 */
	public Coordonnee getCoordonnee() {
		return coordonnee;
	}

}
