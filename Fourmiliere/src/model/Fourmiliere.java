package model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import model.role.Reine;
import model.role.RoleEnum;

/**
 * Classe repr�sentant les fourmili�res existantes sur la carte
 * 
 * @author Gurvan & Samuel
 *
 */
public class Fourmiliere {

	/**
	 * Rayon de la zone dans laquelle la fourmi connait la position de la
	 * fourmiliere
	 */
	private static int rayon = 1000;

	/**
	 * Liste des d�chets pr�sents dans la fourmili�re
	 */
	private List<EtreVivant> dechetsFourmiliere;

	/**
	 * Liste de proies � manger dans la fourmili�re
	 */
	private List<Proie> proieAManger;

	/**
	 * Liste des fourmis vivantes appartenant � la fourmili�re
	 */
	private List<Fourmi> fourmisVivante;

	/**
	 * Liste des ouvri�res appartenant � la fourmili�re
	 */
	private List<Fourmi> ouvrieresVivantes;

	/**
	 * Liste des soldats appartenant � la fourmili�re
	 */
	private List<Fourmi> soldatsVivantes;

	/**
	 * Liste des sexu�s appartenant � la fourmili�re
	 */
	private List<Fourmi> sexuesVivantes;

	/**
	 * Liste des oeufs pr�sents dans la fourmili�re
	 */
	private List<Fourmi> oeufsFourmis;

	/**
	 * Liste des larves pr�sentes dans la fourmili�re
	 */
	private List<Fourmi> larvesFourmis;

	/**
	 * Liste des nymphes pr�sentes dans la fourmili�re
	 */
	private List<Fourmi> nymphesFourmis;

	/**
	 * Les fourmis adultes appartenant � la fourmili�re
	 */
	private List<Fourmi> adultesFourmis;

	/**
	 * Les cadavres pr�sents dans la fourmili�re
	 */
	private List<Fourmi> cadavresFourmis;

	/**
	 * La reine de la fourmili�re
	 */
	private Fourmi reine;

	/**
	 * Les coordonn�es de la fourmili�re sur la carte
	 */
	private Coordonnee coordonnee;

	/**
	 * Constructeur de la fourmili�re.
	 * 
	 * @param reine La reine de la fourmili�re
	 */
	public Fourmiliere(Fourmi reine) {
		this.reine = reine;

		// Initialisation des coordonn�es, elles sont �gales � celles de la reine de la
		// fourmili�re
		this.coordonnee = new Coordonnee(reine.getX(), reine.getY());

		// Initialisation des diff�rentes listes
		this.oeufsFourmis = new ArrayList<>();
		this.larvesFourmis = new ArrayList<>();
		this.nymphesFourmis = new ArrayList<>();
		this.adultesFourmis = new ArrayList<>();
		this.cadavresFourmis = new ArrayList<>();
		this.ouvrieresVivantes = new ArrayList<>();
		this.soldatsVivantes = new ArrayList<>();
		this.sexuesVivantes = new ArrayList<>();
		this.fourmisVivante = new ArrayList<>();
		this.dechetsFourmiliere = new ArrayList<>();
		this.proieAManger = new ArrayList<>();
	}

	/**
	 * R�cup�ration de la liste des fourmis vivantes
	 * 
	 * @return La liste des fourmis vivantes
	 */
	public List<Fourmi> getFourmisVivante() {
		return fourmisVivante;
	}

	/**
	 * R�cup�ration de la liste des proies � manger
	 * 
	 * @return La liste des proies � manger
	 */
	public List<Proie> getProieAManger() {
		return proieAManger;
	}

	/**
	 * Passage des �tapes des fourmili�res
	 * 
	 * @param simulateur
	 * @throws Exception
	 */
	public void step(Simulateur simulateur) throws Exception {
		// Ponte des oeufs pendant le printemps
		if (simulateur.getSaison() == Saison.PRINTEMPS) {
			// Ponte par jour
			if (Simulateur.getHeures() % 24 == 0)
				this.pontePrintemps();
		}

		// Changement d'�tape des fourmis
		this.stepFourmis(simulateur);
	}

	/**
	 * Appel au passage des �tapes des fourmis
	 * 
	 * @param simulateur
	 */
	private void stepFourmis(Simulateur simulateur) {
		for (int i = 0; i < this.getFourmisVivante().size(); i++) {
			Fourmi fourmi = this.getFourmisVivante().get(i);
			fourmi.step(this, simulateur);
		}
	}

	/**
	 * Lance la ponte des oeufs par la reine
	 * 
	 * @throws Exception
	 */
	private void pontePrintemps() {
		Reine reine = (Reine) this.reine.getRole();
		reine.pondre(this);
	}

	/**
	 * R�cup�ration du nombre d'oeuf de la fourmili�re
	 * 
	 * @return Le nombre d'oeuf
	 */
	public int getNbOeufs() {
		return this.getOeufsFourmis().size();
	}

	/**
	 * R�cup�ration du nombre de larve de la fourmili�re
	 * 
	 * @return Le nombre de larve
	 */
	public int getNbLarves() {
		return this.getLarvesFourmis().size();
	}

	/**
	 * R�cup�ration du nombre de nymphe de la fourmili�re
	 * 
	 * @return Le nombre de nymphe
	 */
	public int getNbNymphes() {
		return this.getNymphesFourmis().size();
	}

	/**
	 * R�cup�ration du nombre d'adulte de la fourmili�re
	 * 
	 * @return Le nombre d'adulte
	 */
	public int getNbAdultes() {
		return this.getAdultesFourmis().size();
	}

	/**
	 * R�cup�ration du nombre de cadavre de la fourmili�re
	 * 
	 * @return Le nombre de cadavre
	 */
	public int getNbCadavres() {
		return this.getCadavresFourmis().size();
	}

	/**
	 * R�cup�ration des r�les de chaque fourmi de la fourmili�re
	 * 
	 * @return Une map contenant les r�le de la fourmili�re et leur nombre
	 */
	public Map<RoleEnum, Integer> statsRoles() {
		Map<RoleEnum, Integer> stats = new HashMap<RoleEnum, Integer>();

		stats.put(RoleEnum.OUVRIERE, this.getNbOuvrieres());
		stats.put(RoleEnum.SEXUE, this.getNbSexues());
		stats.put(RoleEnum.SOLDAT, this.getNbSoldats());

		return stats;
	}

	/**
	 * Permet de retirer une fourmi de la totalit� des listes de la fourmili�re
	 * 
	 * @param fourmi
	 */
	public void retirerFourmiListe(Fourmi fourmi) {
		this.fourmisVivante.remove(fourmi);
		this.ouvrieresVivantes.remove(fourmi);
		this.soldatsVivantes.remove(fourmi);
		this.sexuesVivantes.remove(fourmi);
	}

	/**
	 * R�cup�ration du nombre de fourmi ouvri�re de la fourmiliere
	 * 
	 * @return Le nombre d'ouvri�re
	 */
	public int getNbOuvrieres() {
		return this.getOuvrieresVivantes().size();
	}

	/**
	 * R�cup�ration du nombre de fourmi soldat de la fourmiliere
	 * 
	 * @return Le nombre de soldat
	 */
	public int getNbSoldats() {
		return this.getSoldatsVivantes().size();
	}

	/**
	 * R�cup�ration du nombre de fourmi sexu�e de la fourmiliere
	 * 
	 * @return Le nombre de sexu�e
	 */
	public int getNbSexues() {
		return this.getSexuesVivantes().size();
	}

	/**
	 * R�cup�ration du point de l'axe des abscisses p� se trouve la fourmili�re
	 * 
	 * @return Le point d'abscisse
	 */
	public int getX() {
		return this.getCoordonnee().getX();
	}

	/**
	 * R�cup�ration du point de l'axe des ordonn�es p� se trouve la fourmili�re
	 * 
	 * @return Le point d'ordonn�e
	 */
	public int getY() {
		return this.getCoordonnee().getY();
	}

	/**
	 * R�cup�ration de la liste des cadavres de fourmi
	 * 
	 * @return La liste des cadavres
	 */
	public List<Fourmi> getCadavresFourmis() {
		return cadavresFourmis;
	}

	/**
	 * R�cup�ration des coordonn�es de la fourmili�re
	 * 
	 * @return Les coordonn�es
	 */
	public Coordonnee getCoordonnee() {
		return coordonnee;
	}

	/**
	 * R�cup�ration du rayon dans lequel les fourmis on connaissance de la position
	 * de la fourmili�re
	 * 
	 * @return Le rayon
	 */
	public static int getRayon() {
		return rayon;
	}

	/**
	 * R�cup�ration de la liste des fourmis ouvri�res vivantes
	 * 
	 * @return La liste des ouvri�res vivantes
	 */
	public List<Fourmi> getOuvrieresVivantes() {
		return ouvrieresVivantes;
	}

	/**
	 * R�cup�ration de la liste des fourmis soldats vivantes
	 * 
	 * @return La liste des soldats vivantes
	 */
	public List<Fourmi> getSoldatsVivantes() {
		return soldatsVivantes;
	}

	/**
	 * R�cup�ration de la liste des fourmis sexu�es vivantes
	 * 
	 * @return La liste des sexu�es vivantes
	 */
	public List<Fourmi> getSexuesVivantes() {
		return sexuesVivantes;
	}

	/**
	 * R�cup�ration de la liste des oeufs
	 * 
	 * @return La liste des oeufs
	 */
	public List<Fourmi> getOeufsFourmis() {
		return oeufsFourmis;
	}

	/**
	 * R�cup�ration de la liste des larves
	 * 
	 * @return La liste des larves
	 */
	public List<Fourmi> getLarvesFourmis() {
		return larvesFourmis;
	}

	/**
	 * R�cup�ration de la liste des nymphes
	 * 
	 * @return La liste des nymphes
	 */
	public List<Fourmi> getNymphesFourmis() {
		return nymphesFourmis;
	}

	/**
	 * R�cup�ration de la liste des adultes
	 * 
	 * @return La liste des adultes
	 */
	public List<Fourmi> getAdultesFourmis() {
		return adultesFourmis;
	}

}
