package model;

/**
 * Ph�romones d'observation d�pos�es par les ouvri�res sur la carte.
 * 
 * @author Gurvan & Samuel
 *
 */
public class PheromoneObservation extends Pheromone {

	/**
	 * Constructeur des ph�romones d'observation
	 * 
	 * @param heureCreation Heure de cr�ation de la ph�romone
	 */
	public PheromoneObservation(int heureCreation) {
		super(heureCreation);
		PheromoneObservation.validite = 140;
	}

}
