package model.role;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;

import model.Coordonnee;
import model.Direction;
import model.Fourmi;
import model.Fourmiliere;
import model.Proie;
import model.Simulateur;
import model.Terrain;

/**
 * Fourmi portant le role d'ouvri�re. Les fourmis portant le r�le d'ouvri�res
 * chassent les proies afin de nourrir les autres fourmis, elles se d�placent
 * sur toutes la carte en laissant des ph�romones derri�re elles afin de guider
 * le d�placement des autres fourmis.
 * 
 * @author Gurvan & Samuel
 *
 */
public class Ouvriere extends Role {

	/**
	 * Dur�e maximale d'un combat avant la fuite dela fourmi ouvri�re
	 */
	private static final int DUREE_MAX_COMBAT = 1000;

	/**
	 * Date de d�but du combat
	 */
	private int dateDebutCombat;

	/**
	 * Cible courante � laquelle la fourmi s'attaque
	 */
	private Proie cibleCourante;

	/**
	 * Cible � laquelle la fourmi s'est attaqu�e pr�c�demment
	 */
	private Proie ciblePrecedente;

	/**
	 * Proie vaincu que la fourmi ouvri�re doit ramener dans sa fourmili�re
	 */
	private Proie aRamener;

	/**
	 * Constructeur de la fourmi ouvri�re
	 */
	public Ouvriere() {
		this.distanceMax = 200;
	}

	@Override
	public RoleEnum getRoleEnum() {
		return RoleEnum.OUVRIERE;
	}

	/**
	 * Permet le d�placement des fourmis sur la carte dans un p�rim�tre maximum
	 * donn�. Cette m�thode n'est pas testable � cause de METRE_BLOCS
	 * 
	 * @param fourmi
	 * @param fourmiliere
	 * @param terrain
	 */
	public void seDeplacer(Fourmi fourmi, Fourmiliere fourmiliere, Terrain terrain) {
		// R�cup�ration des directions possibles
		List<Direction> directionsPossible = this.directionsPossible(fourmi, fourmiliere, terrain);

		// R�cup�ration des ratios des diff�rentes directions possibles dans le but
		// d'�tablir celle qui sera choisi
		Map<Direction, Double> directionsRatio = this.getRatioDirection(directionsPossible, fourmi, terrain);

		Random random = new Random();
		double valeur = random.nextDouble();
		double totalParcouru = 0.0;
		Direction directionChoisie = null;
		Iterator<Direction> ite = directionsRatio.keySet().iterator();

		while (ite.hasNext() && totalParcouru < valeur) {
			directionChoisie = ite.next();
			totalParcouru += directionsRatio.get(directionChoisie);
		}

		// Ajout des ph�romones de l'ouvri�re
		terrain.addPheromoneOuvriere(fourmi.getX(), fourmi.getY());

		// On fait le deplacement en X
		fourmi.setX(fourmi.getX() + directionChoisie.getDeplacementX());
		// On fait le deplacement en Y
		fourmi.setY(fourmi.getY() + directionChoisie.getDeplacementY());

	}

	/**
	 * Verifier si une proie est sur la meme case que la fourmi
	 * 
	 * @param fourmi
	 * @param terrain
	 */
	private void verifierProximiteProie(Fourmi fourmi, Fourmiliere fourmiliere, Terrain terrain) {
		for (Proie proie : terrain.getListeProies()) {
			// Si la fourmi arrive sur une des proies existantes
			if (proie.getCoordonnee().equals(fourmi.getCoordonnee())) {

				// Si la proie n'a pas d�j� �t� attaqu�e par cette fourmi, on s'ajoute en tant
				// qu'ennemie
				if (!proie.equals(this.ciblePrecedente))
					proie.ajouterEnnemie(fourmi, fourmiliere);

				// On marque le d�but du combat
				this.dateDebutCombat = Simulateur.getHeures();
			}
		}
	}

	/**
	 * Cette m�thode va permettre d'associer un ratio de priorit� de suivi sur les
	 * d�placements disponibles
	 * 
	 * @param directionsPossible Les directions possibles
	 * @param fourmi
	 * @param terrain
	 * @return Chaque direction possible avec un ratio associ�
	 */
	private Map<Direction, Double> getRatioDirection(List<Direction> directionsPossible, Fourmi fourmi,
			Terrain terrain) {

		Map<Direction, Double> directionsRatio = new HashMap<>();

		// On �tabli une liste de directions possibles contenant des ph�romones
		List<Direction> directionPheromone = new ArrayList<>();
		for (Direction direction : directionsPossible) {
			Coordonnee coord = new Coordonnee(fourmi.getX() + direction.getDeplacementX(),
					fourmi.getY() + direction.getDeplacementY());
			if (terrain.getListePheromone().containsKey(coord))
				directionPheromone.add(direction);
		}

		// On �tablie une liste contenant les directions possibles sans ph�romones
		List<Direction> directionNonPheromone = new ArrayList<>(directionsPossible);
		directionNonPheromone.removeAll(directionPheromone);

		double ratioMoy = 1.0 / (double) directionsPossible.size();
		double ratioNonMarque = ratioMoy * (double) directionNonPheromone.size() / directionsPossible.size();
		double ratioMarque = ratioMoy + ratioNonMarque;

		// Pour chacune des directions choisies, on va pr�ciser le pourcentage de chance
		// pour lequel la fourmi va se d�placer dans cette case
		for (Direction direction : directionsPossible) {
			if (directionPheromone.contains(direction)) {
				directionsRatio.put(direction, ratioMarque);
			} else if (directionNonPheromone.contains(direction)) {
				directionsRatio.put(direction, ratioNonMarque);
			}
		}

		return directionsRatio;
	}

	/**
	 * Permet de retourner une liste contenant les directions possibles d'une fourmi
	 * 
	 * @param fourmi
	 * @param fourmiliere
	 * @return Les directions possibles
	 */
	private List<Direction> directionsPossible(Fourmi fourmi, Fourmiliere fourmiliere, Terrain terrain) {
		List<Direction> directionsPossible = new ArrayList<>();

		if (verifierDeplacement(fourmi, fourmiliere, Direction.HAUT))
			directionsPossible.add(Direction.HAUT);

		if (verifierDeplacement(fourmi, fourmiliere, Direction.BAS))
			directionsPossible.add(Direction.BAS);

		if (verifierDeplacement(fourmi, fourmiliere, Direction.GAUCHE))
			directionsPossible.add(Direction.GAUCHE);

		if (verifierDeplacement(fourmi, fourmiliere, Direction.DROITE))
			directionsPossible.add(Direction.DROITE);

		return directionsPossible;
	}

	/**
	 * Verifier les d�placements possibles dans le rayon max de la fourmili�re
	 * 
	 * @param fourmi
	 * @param fourmiliere
	 * @param direction   La direction � v�rifier
	 * @return Vrai si la direction pass�e en param�tre est correcte pour la fourmi
	 *         concern�e, faux sinon
	 */
	private boolean verifierDeplacement(Fourmi fourmi, Fourmiliere fourmiliere, Direction direction) {
		int distanceX = fourmiliere.getX() - (fourmi.getX() + direction.getDeplacementX());
		int distanceY = fourmiliere.getY() - (fourmi.getY() + direction.getDeplacementY());

		distanceX = distanceX * Terrain.METRE_BLOCS;
		distanceY = distanceY * Terrain.METRE_BLOCS;

		double distance = distanceX * distanceX + distanceY * distanceY;

		distance = Math.sqrt(distance);

		if (distance < this.getDistanceMax())
			return true;

		return false;
	}

	/**
	 * Permet de savoir si une fourmi doit abandonner le combat avec une proie, i.e.
	 * si le combat � d�but� il y a 3 heures
	 * 
	 * @param fourmi
	 */
	private void verifierRelacher(Fourmi fourmi, Fourmiliere fourmiliere) {
		if (this.dateDebutCombat + Ouvriere.DUREE_MAX_COMBAT < Simulateur.getHeures())
			this.cibleCourante.retirerEnnemie(fourmi, fourmiliere);
	}

	/**
	 * Permet de commander � une fourmi qui porte une proie qu'elle a chass�e de la
	 * d�pos�e � la fourmili�re. La fourmi ne se dirigera vers la fourmili�re que
	 * lorsqu'elle aura atteint le rayon de d�tection de cette derni�re, avant cela,
	 * elle se d�place al�atoirement
	 * 
	 * @param fourmi
	 * @param terrain
	 * @param fourmiliere
	 */
	private void ramener(Fourmi fourmi, Terrain terrain, Fourmiliere fourmiliere) {
		int distanceFourmiFourmiliereX = fourmi.getX() - fourmiliere.getX();
		int distanceFourmiFourmiliereY = fourmi.getY() - fourmiliere.getY();

		// Si elle se trouve dans le rayon de d�tection de la fourmili�re
		if (Math.abs(distanceFourmiFourmiliereX) <= Fourmiliere.getRayon()
				&& Math.abs(distanceFourmiFourmiliereY) <= Fourmiliere.getRayon()) {

			// move X
			if (fourmi.getX() < fourmiliere.getX())
				fourmi.setX(fourmi.getX() + 1);
			else if (fourmi.getX() > fourmiliere.getX())
				fourmi.setX(fourmi.getX() - 1);

			// move Y
			if (fourmi.getY() < fourmiliere.getY())
				fourmi.setY(fourmi.getY() + 1);
			else if (fourmi.getY() > fourmiliere.getY())
				fourmi.setY(fourmi.getY() - 1);

			this.aRamener.setX(fourmi.getX());
			this.aRamener.setY(fourmi.getY());

			if (aRamener.getCoordonnee().equals(fourmiliere.getCoordonnee())) {
				terrain.getListeProies().remove(this.aRamener);
				fourmiliere.getProieAManger().add(this.aRamener);
				this.aRamener = null;
			}

		} else {
			this.seDeplacer(fourmi, fourmiliere, terrain);
		}
	}

	/**
	 * Si la fourmi meurt, d'une mani�re ou d'une autre, durant un combat contre une
	 * proie, on la retire de la liste des ennemies de la proie
	 * 
	 * @param fourmi
	 * @param fourmiliere
	 */
	private void mortePendantCombat(Fourmi fourmi, Fourmiliere fourmiliere) {
		if (fourmiliere.getCadavresFourmis().contains(fourmi)) {
			this.cibleCourante.retirerEnnemie(fourmi, fourmiliere);
		}
	}

	/**
	 * Mise � jour de la cible courante
	 * 
	 * @param proie
	 */
	public void setCibleCourante(Proie proie) {
		this.cibleCourante = proie;
	}

	/**
	 * Mise � jour de la cible pr�c�dente
	 * 
	 * @param proie
	 */
	public void setCiblePrecedente(Proie proie) {
		this.ciblePrecedente = proie;
	}

	public void setARamener(Proie proie) {
		this.aRamener = proie;
	}

	@Override
	public void step(Fourmi fourmi, Fourmiliere fourmiliere, Terrain terrain) {

		// On se deplace juste
		if (this.cibleCourante == null && this.aRamener == null) {
			verifierProximiteProie(fourmi, fourmiliere, terrain);
			this.seDeplacer(fourmi, fourmiliere, terrain);
			verifierProximiteProie(fourmi, fourmiliere, terrain);
		}

		// On ramener proie battue
		if (this.aRamener != null && this.cibleCourante == null) {
			this.ramener(fourmi, terrain, fourmiliere);
		}

		// On combat
		if (this.cibleCourante != null && this.aRamener == null) {
			this.verifierRelacher(fourmi, fourmiliere);
			this.mortePendantCombat(fourmi, fourmiliere);
		}

	}

}
