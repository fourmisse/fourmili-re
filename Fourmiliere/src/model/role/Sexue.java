package model.role;

import model.Fourmi;
import model.Fourmiliere;
import model.Terrain;

/**
 * Fourmi portant le role de sexu�. Les fourmis portant le r�le de sexu� ne se
 * d�place hors de la fourmili�re que pour le d�p�t des d�chets dans un premier
 * temps. � la fin de l'�t�, les femelles s'envolent en d�posant des ph�romones
 * dans le but de cr�er une nouvelle fourmili�re plus loin.
 * 
 * @author Gurvan & Samuel
 *
 */
public class Sexue extends Role {

	/**
	 * Sexe de la fourmi
	 */
	private boolean estMale;

	/**
	 * Constructeur de la fourmi sexue
	 */
	public Sexue() {
		this.distanceMax = 1000;
	}

	/**
	 * Permet de r�cup�rer le sexe de la fourmi
	 * 
	 * @return Vrai si la fourmi sexue est un male, faux sinosi c'est une femelle
	 */
	public boolean estMale() {
		return estMale;
	}

	/**
	 * Permet de changer le sexe de la fourmi
	 * 
	 * @param estMale Vrai si la fourmi doit devenir un male, faux si elle doit
	 *                devenir une femelle
	 */
	public void setMale(boolean estMale) {
		this.estMale = estMale;
	}

	@Override
	public RoleEnum getRoleEnum() {
		return RoleEnum.SEXUE;
	}

	@Override
	public void seDeplacer(Fourmi fourmi, Fourmiliere fourmiliere, Terrain terrain) {
		// TODO
	}

}
