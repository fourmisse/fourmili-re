package model.role;

import model.Fourmi;
import model.Fourmiliere;
import model.Terrain;

/**
 * Fourmi portant le r�le de soldat. Les fourmis soldats restent pr�s de la
 * fourmili�re dans le but de la d�fendre.
 * 
 * @author Gurvan & Samuel
 *
 */
public class Soldat extends Role {

	/**
	 * COnstructeur de la fourmi soldat
	 */
	public Soldat() {
		this.distanceMax = 50;
	}

	@Override
	public RoleEnum getRoleEnum() {
		return RoleEnum.SOLDAT;
	}

	@Override
	public void seDeplacer(Fourmi fourmi, Fourmiliere fourmiliere, Terrain terrain) {
		// TODO Auto-generated method stub
	}

}
