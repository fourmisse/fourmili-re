package model.role;

import java.util.Random;

import model.Fourmi;
import model.Fourmiliere;
import model.Terrain;

/**
 * Fourmi portant le role de reine. Les fourmis portant le r�le de reine ne se
 * d�place pas. Son r�le consiste uniquement � la ponte d'oeuf. De plus, elle
 * vit plus longtemps que les autres fourmis.
 * 
 * @author Gurvan & Samuel
 *
 */
public class Reine extends Role {

	/**
	 * Ponte maximale possible de la reine par jour
	 */
	private static final int ponteMax = 5;

	/**
	 * Ponte minimale possible de la reine par jour
	 */
	private static final int ponteMin = 1;

	/**
	 * Constructeur de la reine
	 * 
	 * @param fourmi
	 * @param terrain
	 */
	public Reine(Fourmi fourmi, Terrain terrain) {
		super();
		Random random = new Random(System.currentTimeMillis());
		fourmi.getEtat().setDuree((int)(random.nextDouble()*6+4)*365*24);
		this.distanceMax = 0;
		this.creerFourmiliere(fourmi, terrain);
	}

	/**
	 * Permet la mise en place de la ponte de la reine
	 * 
	 * @param fourmiliere
	 */
	public void pondre(Fourmiliere fourmiliere) {

		int nbOeufsParJour = (int) ((Math.random() * (ponteMax - ponteMin)) + ponteMin);
//		int nbOeufsParHeure = (int) (nbOeufsParJour / 24);

		for (int i = 0; i < nbOeufsParJour; i++) {
			Fourmi fourmi = new Fourmi(fourmiliere.getX(), fourmiliere.getY());
			fourmiliere.getFourmisVivante().add(fourmi);
			fourmiliere.getOeufsFourmis().add(fourmi);
		}

	}

	/**
	 * Le d�placement de la reine ne doit � aucun moment �tre effectu�
	 */
	public void seDeplacer() {
		return;
	}

	/**
	 * Permet la cr�ation de la fourmili�re associ�e � la reine
	 * 
	 * @param fourmi
	 * @param terrain
	 * @return La fourmili�re cr�e
	 */
	public Fourmiliere creerFourmiliere(Fourmi fourmi, Terrain terrain) {
		Fourmiliere fourmiliere = new Fourmiliere(fourmi);
		terrain.getListeFourmiliere().add(fourmiliere);

		return fourmiliere;
	}

	@Override
	public RoleEnum getRoleEnum() {
		return RoleEnum.REINE;
	}

	@Override
	public void seDeplacer(Fourmi fourmi, Fourmiliere fourmiliere, Terrain terrain) {
		return;
	}
}
