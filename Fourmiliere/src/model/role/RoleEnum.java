package model.role;

/**
 * Permet de r�cup�rer les ratios minimums et maximums de la pr�sence de chaque
 * r�le des fourmis dans la fourmili�re.
 * 
 * @author Gurvan & Samuel
 *
 */
public enum RoleEnum {
	SOLDAT(0.2, 0.25), OUVRIERE(0.6, 0.7), SEXUE(0.05, 0.2), REINE(0.0, 0.0);

	/**
	 * Ratio minimum de pr�sence du role de la fourmi dans la fourmili�re
	 */
	private double ratioMin;

	/**
	 * Ratio maximum de pr�sence du role de la fourmi dans la fourmili�re
	 */
	private double ratioMax;

	/**
	 * Constructeur de l'�num�ration des r�les
	 * 
	 * @param ratioMin
	 * @param ratioMax
	 */
	RoleEnum(double ratioMin, double ratioMax) {
		this.ratioMin = ratioMin;
		this.ratioMax = ratioMax;
	}

	/**
	 * R�cup�ration du ratio minimum du role
	 * 
	 * @return Le ratio minimum
	 */
	public double getRatioMin() {
		return ratioMin;
	}

	/**
	 * R�cup�ration du ratio maximum du role
	 * 
	 * @return Le ratio maximum
	 */
	public double getRatioMax() {
		return ratioMax;
	}

}
