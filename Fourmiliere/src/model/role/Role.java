package model.role;

import model.Fourmi;
import model.Fourmiliere;
import model.Terrain;

/**
 * Classe abstraite d�finissant le r�le des fourmis de la fourmili�re.
 * 
 * @author Gurvan & Samuel
 *
 */
public abstract class Role {

	/**
	 * Distance maximale qu'une fourmi ne peut d�passer en partant de sa fourmili�re
	 */
	protected int distanceMax;

	/**
	 * Permet de r�cup�rer le role d'une fourmi adulte
	 * 
	 * @return Le role de la fourmi concern�e
	 */
	public abstract RoleEnum getRoleEnum();

	/**
	 * Chaque s�quence de la simulation va d�clencher une suite d'�v�nements
	 * 
	 * @param fourmi
	 * @param fourmiliere
	 * @param terrain
	 */
	public void step(Fourmi fourmi, Fourmiliere fourmiliere, Terrain terrain) {
		this.seDeplacer(fourmi, fourmiliere, terrain);
	}

	/**
	 * Permet le d�placement de la fourmi
	 * 
	 * @param fourmi
	 * @param fourmiliere
	 * @param terrain
	 */
	public abstract void seDeplacer(Fourmi fourmi, Fourmiliere fourmiliere, Terrain terrain);

	public int getDistanceMax() {
		return distanceMax;
	}

}
