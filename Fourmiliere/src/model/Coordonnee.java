package model;

/**
 * Classe coordonnee qui permet de donner des coordonnees aux diff�rents
 * �l�ments de la simulation. Impl�mente l'interface Cloneable.
 * 
 * @author Gurvan & Samuel
 *
 */
public class Coordonnee implements Cloneable {
	private int x;
	private int y;

	/**
	 * Constructeur de la classe coordonn�es
	 * 
	 * @param x
	 * @param y
	 */
	public Coordonnee(int x, int y) {
		this.x = x;
		this.y = y;
	}

	@Override
	public boolean equals(Object o) {
		if (o == null)
			return false;
		if (!(o instanceof Coordonnee))
			return false;

		Coordonnee coord = (Coordonnee) o;

		if (this.getX() == coord.getX() && this.getY() == coord.getY()) {
			return true;
		}

		return false;
	}

	@Override
	public int hashCode() {
		return this.x + this.y;
	}

	@Override
	public String toString() {
		return "(" + getX() + "x" + getY() + ")";
	}

	/**
	 * R�cup�ration du point d'abscisse
	 * 
	 * @return Le point d'abscisse
	 */
	public int getX() {
		return x;
	}

	/**
	 * Mise � jour du point d'abscisse
	 * 
	 * @param x Le nouveau point d'abscisse � appliquer
	 */
	public void setX(int x) {
		this.x = x;
	}

	/**
	 * R�cup�ration du point d'ordonn�e
	 * 
	 * @return Le point d'ordonn�e
	 */
	public int getY() {
		return y;
	}

	/**
	 * Mise � jour du point d'ordonn�e
	 * 
	 * @param y Le nouveau point d'ordonn�e
	 */
	public void setY(int y) {
		this.y = y;
	}

	@Override
	public Object clone() {
		return new Coordonnee(this.getX(), this.getY());
	}

}
