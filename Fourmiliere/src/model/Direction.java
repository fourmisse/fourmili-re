package model;

/**
 * Énumération des déplacements possibles.
 * 
 * @author Gurvan & Samuel
 *
 */
public enum Direction {
	HAUT(0, -1), BAS(0, 1), GAUCHE(-1, 0), DROITE(1, 0);

	/**
	 * Déplacement dans l'axe des abscisses
	 */
	private int deplacementX;

	/**
	 * Déplacement dans l'axe des ordonnées
	 */
	private int deplacementY;

	/**
	 * Constructeur de Direction
	 * 
	 * @param deplacementX
	 * @param deplacementY
	 */
	Direction(int deplacementX, int deplacementY) {
		this.deplacementX = deplacementX;
		this.deplacementY = deplacementY;
	}

	/**
	 * Récupération du point de l'axe des abscisses
	 * 
	 * @return Le point de l'axe des abscisses
	 */
	public int getDeplacementX() {
		return this.deplacementX;
	}

	/**
	 * Récupération du point de l'axe des ordonnées
	 * 
	 * @return Le point de l'axe des ordonnées
	 */
	public int getDeplacementY() {
		return this.deplacementY;
	}
}
