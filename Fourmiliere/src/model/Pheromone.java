package model;

/**
 * Classe abstraite repr�sentant les ph�romones laiss�s par les fourmis.
 * 
 * @author Gurvan & Samuel
 *
 */
public abstract class Pheromone {

	/**
	 * Dur�e de validit� de la ph�romone
	 */
	protected static int validite;
	
	/**
	 * Heure depuis laquelle la ph�romone a �t� cr��e
	 */
	protected int heureCreation;

	/**
	 * Constructeur de ph�romone
	 * @param heureCreation
	 */
	public Pheromone(int heureCreation) {
		this.heureCreation = heureCreation;
	}

	/**
	 * V�rification de la validit� de la ph�romone
	 * @return Vrai si la ph�romone est toujours valide, faux sinon
	 */
	public boolean isValid() {
		if (this.heureCreation + Pheromone.validite < Simulateur.getHeures())
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Pheromone(" + heureCreation + ")";
	}

}
