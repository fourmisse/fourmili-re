package model;

import java.util.HashSet;
import java.util.Random;
import java.util.Set;

import model.role.Ouvriere;
import model.role.Role;
import view.Carte;

/**
 * Classe repr�sentant les proies pr�sentent sur la carte
 * 
 * @author Gurvan & Samuel
 *
 */
public class Proie extends EtreVivant {

	/**
	 * Les coordonn�es de la proie sur la carte
	 */
	private Coordonnee coordonnee;

	/**
	 * Liste des fourmis qui attaquent
	 */
	private Set<Role> ennemies;

	/**
	 * �tat de la proie. Vrai si elle est vaicue, faux sinon
	 */
	private boolean vaincue;

	/**
	 * Poids total de tous les ennemies, si il d�passe le poids de la proie, elle
	 * meurt
	 */
	private double poidsTotalEnnemies;

	/**
	 * Date de d�but du combat contre les fourmis
	 */
	private int dateDebutCombat;

	/**
	 * Dur�e maximale de combat. Une fois celui-ci d�pass�, la proie s'enfuit
	 */
	private static final int DUREE_MAX_COMBAT = 1000;

	/**
	 * Constructeur de proie
	 * 
	 * @param terrain
	 */
	public Proie(Terrain terrain) {
		// Apparition de la proie sur un point al�atoire de la carte
		Random random = new Random(System.currentTimeMillis());
		int x = random.nextInt(Carte.LONGUEUR);
		int y = random.nextInt(Carte.LARGEUR);
		this.coordonnee = new Coordonnee(x, y);

		// Initialisation du poids de la proie, le maximum qu'elle peut atteindre est 65
		// mg
		this.setPoids(random.nextDouble() * 65);

		// Ajout de la proie au terrain
		terrain.getListeProies().add(this);

		// Initialisation de la liste des ennemies
		this.ennemies = new HashSet<>();
	}

	/**
	 * Permet l'ajout d'un ennemi. Cette m�thode est utilis� quand une fourmi
	 * rejoint le combat
	 * 
	 * @param fourmi
	 * @param fourmiliere
	 */
	public void ajouterEnnemie(Fourmi fourmi, Fourmiliere fourmiliere) {
		// Si c'est la premi�re fourmi � attaquer, on donne le d�but du combat
		if (this.ennemies.size() == 0)
			this.dateDebutCombat = Simulateur.getHeures();

		// On ajoute la fourmie � la liste et son poids au poids total de toutes les
		// attaquantes
		if (this.ennemies.add(fourmi.getRole()))
			this.poidsTotalEnnemies += fourmi.getPoids();

		// Pour chacune des fourmi, on ajoute cette proie � ces fourmis
		if (fourmiliere.getOuvrieresVivantes().contains(fourmi)) {
			Ouvriere ouvriere = (Ouvriere) fourmi.getRole();
			ouvriere.setCibleCourante(this);
		}

	}

	/**
	 * Permet de retirer un ennemi. Cette m�thode peut �tre utilis�e quand une
	 * fourmi meurt pendant le combat ou qu'elle fuit
	 * 
	 * @param fourmi
	 * @param fourmiliere
	 */
	public void retirerEnnemie(Fourmi fourmi, Fourmiliere fourmiliere) {
		// On retire la fourmi de la liste des ennemies et son poids du poids total
		if (this.ennemies.remove(fourmi.getRole()))
			this.poidsTotalEnnemies -= fourmi.getPoids();

		// Si il n'y a plus aucun ennemi, on remet la date de d�but du combat � 0
		if (this.ennemies.size() == 0)
			this.dateDebutCombat = 0;

		// On d�signe cette proie en tant que pr�c�dente pour tous les ennemis
		// pr�c�dents
		if (fourmiliere.getOuvrieresVivantes().contains(fourmi)) {
			Ouvriere ouvriere = (Ouvriere) fourmi.getRole();
			ouvriere.setCibleCourante(null);
			ouvriere.setCiblePrecedente(this);
		}
	}

	/**
	 * Permet de v�rifier si le nombre de fourmi attaquant cette proie est suffisant
	 * pour la tuer
	 * 
	 * @return Vrai si le nombre de fourmi est suffisant, faux sinon
	 */
	public boolean verifierBattue() {
		// Si le poids de cette proie est d�passer par le poids total des ennemis
		if (this.poidsTotalEnnemies >= this.getPoids()) {

			// Qu'elle n'est pas encore transport�e par une fourmi
			boolean estTransportee = false;

			for (Role ennemie : this.ennemies) {
				// Seule une ouvri�re pourra transport�e la proie ver la fourmili�re
				if (ennemie instanceof Ouvriere) {
					Ouvriere ennemieOuvriere = (Ouvriere) ennemie;
					// La proie sera transport�e par la premi�re ouvri�re trouv�e
					if (!estTransportee) {
						ennemieOuvriere.setARamener(this);
						estTransportee = true;
					}
					// On remet � null les cibles courantes de chaque attaquantes
					ennemieOuvriere.setCibleCourante(null);
				}
			}
			this.vaincue = true;
			return true;
		}

		return false;
	}

	/**
	 * V�rifie si la proie est en combat depuis assez longtemps pour fuir
	 * 
	 * @return Vrai si la proie peut s'enfuir, faux sinon
	 */
	public boolean verifierFuir() {
		// Si la limite de temps de combat est atteinte
		if (this.dateDebutCombat + Proie.DUREE_MAX_COMBAT < Simulateur.getHeures()) {

			for (Role ennemie : this.ennemies) {
				// Pour chaque fourmi, on passe la cible courante � null et cette proie en tant
				// que cible pr�c�dente
				if (ennemie instanceof Ouvriere) {
					Ouvriere ennemieOuvriere = (Ouvriere) ennemie;
					ennemieOuvriere.setCibleCourante(null);
					ennemieOuvriere.setCiblePrecedente(this);
				}
			}

			// On remet les param�tres concernant les ennemis � 0
			this.ennemies.clear();
			this.poidsTotalEnnemies = 0;
			this.dateDebutCombat = 0;

			return true;
		}

		return false;
	}

	/**
	 * Permet de g�rer le d�placement de la proie. Le d�placement de la proie est
	 * totalement al�atoire
	 */
	private void seDeplacer() {
		double choixDeplacement = Math.random();
		Direction directionChoisie;

		if (choixDeplacement < 0.25)
			directionChoisie = Direction.HAUT;
		else if (choixDeplacement >= 0.25 && choixDeplacement < 0.5)
			directionChoisie = Direction.DROITE;
		else if (choixDeplacement >= 0.5 && choixDeplacement < 0.75)
			directionChoisie = Direction.BAS;
		else
			directionChoisie = Direction.GAUCHE;

		this.setX(this.getX() + directionChoisie.getDeplacementX());
		this.setY(this.getY() + directionChoisie.getDeplacementY());
	}

	/**
	 * �tape de la proie. Si elle n'est pas attaquer, elle peut se d�placer. Sinon,
	 * elle v�rifie si elle meurt ou fuit
	 */
	public void step() {
		if (this.ennemies.size() == 0) {
			seDeplacer();
		} else {
			verifierFuir();
			verifierBattue();
		}
	}

	/**
	 * R�cup�ration des coordonn�es de la proie
	 * 
	 * @return Les coordonn�es de la proie
	 */
	public Coordonnee getCoordonnee() {
		return this.coordonnee;
	}

	/**
	 * R�cup�ration du point de l'axe des abscisses de la proie
	 * 
	 * @return Le point de l'axe des abscisses
	 */
	public int getX() {
		return this.coordonnee.getX();
	}

	/**
	 * Mise � jour du point des abscisses
	 * 
	 * @param x Le nouveau point de l'axe des abscisses
	 */
	public void setX(int x) {
		this.coordonnee.setX(x);
	}

	/**
	 * R�cup�ration du point de l'axe des ordonn�es de la proie
	 * 
	 * @return Le point de l'axe des ordonn�es
	 */
	public int getY() {
		return this.coordonnee.getY();
	}

	/**
	 * Mise � jour du point des ordonn�es
	 * 
	 * @param y Le nouveau point de l'axe des ordonn�es
	 */
	public void setY(int y) {
		this.coordonnee.setY(y);
	}

	/**
	 * R�cup�ration de l'�tat de la proie
	 * 
	 * @return Vrai si la proie est vivante, faux sinon
	 */
	public boolean isVaincue() {
		return vaincue;
	}

}
