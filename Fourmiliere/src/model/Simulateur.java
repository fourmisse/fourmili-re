package model;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;

/**
 * Classe permettant la simulation
 * 
 * @author Gurvan & Samuel
 *
 */
public class Simulateur {

	/**
	 * Nombre d'heures par an
	 */
	private final int nbHeuresParAn = 8760;

	/**
	 * Nombre d'heures pass�es actuellement
	 */
	private static int heures;

	/**
	 * Nombre de ms par heure dans la simulation
	 */
	public static final int NB_MS_HEURE = 1;

	/**
	 * Appel les diff�rents listener de l'application
	 */
	private PropertyChangeSupport pcs;

	/**
	 * Le terrain contenant tous les �l�ments de la simulation
	 */
	private Terrain terrain;

	/**
	 * Constructeur du simulateur
	 */
	public Simulateur() {
		this.pcs = new PropertyChangeSupport(this);
		this.terrain = new Terrain();
	}

	/**
	 * R�cup�ration du nombre d'heures pass�es � l'appel de la m�thode
	 * 
	 * @return Le nombre d'heures pass�es
	 */
	public static int getHeures() {
		return heures;
	}

	/**
	 * Changement d'�tape dans la simulation
	 * 
	 * @throws Exception
	 */
	public void step() throws Exception {
		// Incr�mentation du nombre d'heures pass�es
		Simulateur.heures++;

		// Changement d'�tape des proies
		for (int i = 0; i < this.getTerrain().getListeProies().size(); i++) {
			this.getTerrain().getListeProies().get(i).step();
		}

		// Actionne la fourmiliere
		for (int i = 0; i < this.getTerrain().getListeFourmiliere().size(); i++) {
			this.getTerrain().getListeFourmiliere().get(i).step(this);
		}

		// Ajout de proie, toutes les 6 heures
		if (Simulateur.heures % 6 == 0) {
			new Proie(this.terrain);
		}

		// On avertie les vues du changement d'�tape des diff�rents �l�ments de la
		// simulation
		this.pcs.firePropertyChange("world", null, null);
	}

	/**
	 * Permet de d�terminer la saison courante de l'ann�e
	 * 
	 * @return La saison courante
	 * @throws Exception
	 */
	public Saison getSaison() throws Exception {
		// Heures pass�es dans l'ann�e, bas�es sur le nombre d'heures totales pass�es
		// depuis le d�but de la simulation et le nombre d'heures totales dans une ann�e
		int heuresDansLannee = heures % nbHeuresParAn;

		if (heuresDansLannee <= Saison.PRINTEMPS.getFin())
			return Saison.PRINTEMPS;
		else if (heuresDansLannee > Saison.ETE.getDebut() && heuresDansLannee <= Saison.ETE.getFin())
			return Saison.ETE;
		else if (heuresDansLannee > Saison.AUTOMNE.getDebut() && heuresDansLannee <= Saison.AUTOMNE.getFin())
			return Saison.AUTOMNE;
		else if (heuresDansLannee > Saison.HIVER.getDebut() && heuresDansLannee <= Saison.HIVER.getFin())
			return Saison.HIVER;

		throw new Exception();
	}

	/**
	 * Permet l'ajout d'un listener � cet objet
	 * 
	 * @param listener L'objet qui doit �couter
	 */
	public void addListener(PropertyChangeListener listener) {
		this.pcs.addPropertyChangeListener(listener);
	}

	/**
	 * R�cup�ration du terrain de la simulation
	 * 
	 * @return Le terrain
	 */
	public Terrain getTerrain() {
		return terrain;
	}

}
