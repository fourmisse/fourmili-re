package model;

/**
 * �num�ration repr�sentant les saisons de l'ann�e. Elle contient l'heure de
 * d�but et de fin de chaque saison dans l'ann�e
 * 
 * @author Gurvan & Samuel
 *
 */
public enum Saison {
	PRINTEMPS(0, 2190), ETE(2190, 2190 * 2), AUTOMNE(2190 * 2, 2190 * 3), HIVER(2190 * 3, 2190 * 4);

	/**
	 * Heure de d�but de la saison
	 */
	private int debut;

	/**
	 * Heure de fin de la saison
	 */
	private int fin;

	/**
	 * Constructeur de l'�num�ration des saisons
	 * 
	 * @param debut
	 * @param fin
	 */
	Saison(int debut, int fin) {
		this.debut = debut;
		this.fin = fin;
	}

	/**
	 * R�cup�ration de l'heure de d�but de la saison
	 * 
	 * @return L'heure de d�but de la saison
	 */
	public int getDebut() {
		return this.debut;
	}

	/**
	 * R�cup�ration de l'heure de fin de la saison
	 * 
	 * @return L'heure de fin de la saison
	 */
	public int getFin() {
		return this.fin;
	}

}
