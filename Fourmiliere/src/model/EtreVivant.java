package model;

/**
 * Classe d�finissant les �tres vivants pr�sents dans la simulation.
 * 
 * @author Gurvan & Samuel
 *
 */
public abstract class EtreVivant {

	/**
	 * Le poids de l'�tre vivant en mg
	 */
	private double poids;

	/**
	 * L'esp�rance de vie de l'�tre vivant en heure
	 */
	protected double esperanceVie;

	/**
	 * R�cup�ration de l'esp�rence de vie de l'�tre vivant
	 * 
	 * @return L'esp�rance de vie en heure
	 */
	public double getEsperanceVie() {
		return esperanceVie;
	}

	/**
	 * R�cup�ration du poids de l'�tre vivant
	 * 
	 * @return Le poids de l'�tre en vivant en heure
	 */
	public double getPoids() {
		return poids;
	}

	/**
	 * Mise � jour du poids de l'�tre vivant
	 * 
	 * @param poids Le nouveau poids � appliquer
	 */
	public void setPoids(double poids) {
		this.poids = poids;
	}

}
