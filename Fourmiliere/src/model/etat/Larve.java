package model.etat;

import model.Fourmi;
import model.Fourmiliere;
import model.Terrain;

/**
 * Classe repr�sentant l'�tat de larve d'une fourmie. Suit l'�tat de l'oeuf.
 * Pr�c�de l'�tat de nymphe.
 * 
 * @author Gurvan & Samuel
 *
 */
public class Larve extends EtatFourmi {

	/**
	 * Constructeur de la larve
	 * 
	 * @param fourmi
	 */
	public Larve(Fourmi fourmi) {
		super(fourmi);

		// Dur�e de l'�tat larve : 240 heures
		this.duree = 240;
	}

	/**
	 * Apr�s l'�tat de larve vient l'�tat de nymphe
	 */
	public Nymphe next(Fourmi fourmi, Fourmiliere fourmiliere, Terrain terrain) {
		fourmiliere.getLarvesFourmis().remove(fourmi);
		fourmiliere.getNymphesFourmis().add(fourmi);
		return new Nymphe(fourmi);
	}
}
