package model.etat;

import java.util.Map;
import java.util.Random;

import model.Fourmi;
import model.Fourmiliere;
import model.Terrain;
import model.role.Ouvriere;
import model.role.Reine;
import model.role.Role;
import model.role.RoleEnum;
import model.role.Sexue;
import model.role.Soldat;

/**
 * Classe repr�sentant l'�tat d'adulte d'une fourmi. Vient apr�s l'�tat de
 * nyphe. pr�c�de l'�tat de cadavre.
 * 
 * @author Gurvan & Samuel
 *
 */
public class Adulte extends EtatFourmi {

	/**
	 * Le role de la fourmi adulte
	 */
	private Role role;

	/**
	 * Constructeur de la fourmi adulte
	 * 
	 * @param fourmi
	 * @param fourmiliere
	 * @param terrain
	 */
	public Adulte(Fourmi fourmi, Fourmiliere fourmiliere, Terrain terrain) {
		super(fourmi);
		Random random = new Random();
		this.duree = (int) (random.nextDouble() + 1.5) * 365 * 24;
		choisirRole(fourmi, fourmiliere, terrain);
	}

	@Override
	public Cadavre next(Fourmi fourmi, Fourmiliere fourmiliere, Terrain terrain) {
		fourmiliere.getAdultesFourmis().remove(fourmi);
		fourmiliere.retirerFourmiListe(fourmi);
		fourmiliere.getCadavresFourmis().add(fourmi);
		return new Cadavre(fourmi);
	}

	/**
	 * R�cup�ration du r�le de la fourmi
	 * 
	 * @return Le r�le
	 */
	public Role getRole() {
		return role;
	}

	/**
	 * Choix du r�le de la fourmi lors de son passage � l'�tat d'adulte. Ce choix
	 * est fait de mani�re � respecter les proportions de chaque r�le de fourmi dans
	 * une fourmili�re donn�es dans la consigne
	 * 
	 * @param fourmi
	 * @param fourmiliere
	 * @param terrain
	 */
	public void choisirRole(Fourmi fourmi, Fourmiliere fourmiliere, Terrain terrain) {
		// Si la fourmili�re n'est pas cr��e, cela signifie que nous cr�ons la reine de
		// la fourmili�re
		if (fourmiliere == null)
			this.role = new Reine(fourmi, terrain);

		// Sinon, on va calculer les ratios des r�les les plus manquants dans la
		// fourmili�re
		else {
			// Map contenant les r�les pr�sents dans la fourmili�re associ�s avec leur ratio
			// de pr�sence
			Map<RoleEnum, Integer> stats = fourmiliere.statsRoles();

			// Population totale d'adulte dans la fourmili�re
			double totalPopulationAdulte = fourmiliere.getNbAdultes();

			// Choix du r�le
			RoleEnum role = this.choisirRatio(stats, totalPopulationAdulte);

			// Ajout de la fourmi dans la liste correpondant � son r�le
			if (role == RoleEnum.OUVRIERE) {
				this.role = new Ouvriere();
				fourmiliere.getOuvrieresVivantes().add(fourmi);
			} else if (role == RoleEnum.SOLDAT) {
				this.role = new Soldat();
				fourmiliere.getSoldatsVivantes().add(fourmi);
			} else if (role == RoleEnum.SEXUE) {
				this.role = new Sexue();
				fourmiliere.getSexuesVivantes().add(fourmi);
			}

		}

	}

	/**
	 * On r�cup�re les ratios de chaque r�le pr�sent dans la fourmili�re afin de
	 * d�cider le r�le de la fourmi devenu adulte
	 * 
	 * @param stats                 Map associant chaque r�le avec sa pr�sence dans
	 *                              la fourmili�re
	 * @param totalPopulationAdulte
	 * @return Le r�le de la fourmi
	 */
	private RoleEnum choisirRatio(Map<RoleEnum, Integer> stats, double totalPopulationAdulte) {

		// de 60% � 70%
		double ouvriereMin = RoleEnum.OUVRIERE.getRatioMin();
		// de 20% � 25%
		double soldatMin = RoleEnum.SOLDAT.getRatioMin();
		// de 5 � 20%
		double sexueMin = RoleEnum.SEXUE.getRatioMin();

		// Ratio de chaque r�le permettant de d�terminer par la suite le r�le qui manque
		// le plus � la fourmili�re
		double ratioOuvrieres = stats.get(RoleEnum.OUVRIERE) / totalPopulationAdulte;
		double ratioSoldats = stats.get(RoleEnum.SOLDAT) / totalPopulationAdulte;
		double ratioSexues = stats.get(RoleEnum.SEXUE) / totalPopulationAdulte;

		// Diff�rence entre les diff�rents ratios
		double diffOuvriere = ratioOuvrieres - ouvriereMin;
		double diffSoldat = ratioSoldats - soldatMin;
		double diffSexue = ratioSexues - sexueMin;

		// D�termination du minimum de ces ratios
		double minDiff = Math.min(Math.min(diffOuvriere, diffSoldat), diffSexue);

		Random rand = new Random(System.currentTimeMillis());

		// D�termination du r�le � donner � la nouvelle fourmi adulte
		if (minDiff > 0)
			return RoleEnum.values()[rand.nextInt(3)];
		else if (minDiff == diffOuvriere)
			return RoleEnum.OUVRIERE;
		else if (minDiff == diffSoldat)
			return RoleEnum.SOLDAT;
		else if (minDiff == diffSexue)
			return RoleEnum.SEXUE;

		return null;
	}

	@Override
	public void step(Fourmi fourmi, Fourmiliere fourmiliere, Terrain terrain) {
		this.getRole().step(fourmi, fourmiliere, terrain);
	}

}
