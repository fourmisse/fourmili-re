package model.etat;

import model.Fourmi;
import model.Fourmiliere;
import model.Terrain;

/**
 * Classe repr�sentant l'�tat oeuf d'une fourmie. Premi�re �tat de celle-ci.
 * Pr�c�de l'�tat de larve.
 * 
 * @author Gurvan & Samuel
 *
 */
public class Oeuf extends EtatFourmi {

	/**
	 * Constructeur de l'oeuf
	 * 
	 * @param fourmi
	 */
	public Oeuf(Fourmi fourmi) {
		super(fourmi);

		// Dur�e de l'�tat oeuf : 72 heures
		this.duree = 72;
	}

	/**
	 * Apr�s l'�tat d'oeuf vient l'�tat de larve
	 */
	public Larve next(Fourmi fourmi, Fourmiliere fourmiliere, Terrain terrain) {
		fourmiliere.getOeufsFourmis().remove(fourmi);
		fourmiliere.getLarvesFourmis().add(fourmi);
		return new Larve(fourmi);
	}
}
