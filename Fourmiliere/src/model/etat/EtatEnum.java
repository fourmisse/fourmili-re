package model.etat;

/**
 * �num�ration des �tats diff�rents des fourmis
 * 
 * @author Gurvan & Samuel
 *
 */
public enum EtatEnum {
	OEUF, LARVE, NYMPHE, ADULTE, CADAVRE
}
