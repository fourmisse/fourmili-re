package model.etat;

import model.Fourmi;
import model.Fourmiliere;
import model.Terrain;

/**
 * Classe abstraite repr�sentant l'�tat de la fourmi parmi : oeuf, larve,
 * nymphe, adulte et cadavre
 * 
 * @author Gurvan & Samuel
 *
 */
public abstract class EtatFourmi {

	/**
	 * Dur�e durant laquelle l'�tat de la fourmi sera effectif
	 */
	protected int duree;

	/**
	 * Constructeur de l�tat de la fourmi
	 * 
	 * @param fourmi
	 */
	public EtatFourmi(Fourmi fourmi) {
		fourmi.setEtat(this);
	}

	/**
	 * Permet de passe � l'�tat suivi dans le cycle de la vie d'une fourmi. oeuf ->
	 * larve, larve -> nymphe, nymphe -> adulte, adulte -> cadavre
	 * 
	 * @param fourmi
	 * @param fourmiliere
	 * @param terrain
	 * @return L'�tat apr�s le passage
	 */
	public abstract EtatFourmi next(Fourmi fourmi, Fourmiliere fourmiliere, Terrain terrain);

	/**
	 * R�cup�ration de la dur�e de l'�tat
	 * 
	 * @return La dur�e de l'�tat
	 */
	public int getDuree() {
		return this.duree;
	}

	/**
	 * Mise � jour de la dur�e de l'�tat, notamment utile pour la dur�e de vie de la
	 * reine qui est diff�rente des autres fourmis adultes
	 * 
	 * @param esperance Nouvelle dur�e de l'�tat
	 */
	public void setDuree(int esperance) {
		this.duree = esperance;
	}

	/**
	 * Changement d'�tape de la fourmi
	 * 
	 * @param fourmi
	 * @param fourmiliere
	 * @param terrain
	 */
	public void step(Fourmi fourmi, Fourmiliere fourmiliere, Terrain terrain) {
		return;
	}

}
