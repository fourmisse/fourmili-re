package model.etat;

import model.Fourmi;
import model.Fourmiliere;
import model.Terrain;

/**
 * Classe repr�sentant l'�tat de nymphe d'une fourmie. Premi�re �tat de
 * celle-ci. Pr�c�de l'�tat de d'adulte.
 * 
 * @author Gurvan & Samuel
 *
 */
public class Nymphe extends EtatFourmi {

	/**
	 * Constructeur de la nyphe
	 * 
	 * @param fourmi
	 */
	public Nymphe(Fourmi fourmi) {
		super(fourmi);
		this.duree = 408;
	}

	/**
	 * Apr�s l'�tat de nymphe vient l'�tat d'adulte
	 */
	public Adulte next(Fourmi fourmi, Fourmiliere fourmiliere, Terrain terrain) {
		fourmiliere.getNymphesFourmis().remove(fourmi);
		fourmiliere.getAdultesFourmis().add(fourmi);
		return new Adulte(fourmi, fourmiliere, terrain);
	}
}