package model.etat;

import model.Fourmi;
import model.Fourmiliere;
import model.Terrain;

/**
 * Classe repr�sentant l'�tat de cadavre d'une fourmi. Vient apr�s l'�tat
 * d'adulte. Dernier �tat de la fourmi.
 * 
 * @author Gurvan & Samuel
 *
 */
public class Cadavre extends EtatFourmi {

	/**
	 * Constructeur du cadavre
	 * 
	 * @param fourmi
	 */
	public Cadavre(Fourmi fourmi) {
		super(fourmi);
	}

	@Override
	public EtatFourmi next(Fourmi fourmi, Fourmiliere fourmiliere, Terrain terrain) {

		return null;
	}

}
