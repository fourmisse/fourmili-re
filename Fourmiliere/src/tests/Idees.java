//package tests;
//
//import java.util.ArrayList;
//
//import model.EtreVivant;
//import model.Fourmiliere;
//import model.Proie;
//import model.Simulateur;
//import model.etat.EtatEnum;
//
//public class Idees {
//	
//	/**
//	 * Une autre grille dans carte qui contient toutes les ph�romones
//	 * Ph�romone est une classe
//	 * Elle contient :
//	 * 		Le temps depuis lequel elle est d�pos�e
//	 * 		Le type de ph�romones (constante, enum, ... ?)
//	 * 
//	 */
//
//	// Dans fourmiliere
//	/*
//	 * Liste contenant toutes les proies consommables
//	 * 
//	 * --> Ajouter un getter
//	 */
//	ArrayList<Proie> consommables = new ArrayList<Proie>();
//	
//	/*
//	 * Liste contenant toutes les proies non-consommables
//	 * 
//	 * --> Ajouter un getter
//	 * 
//	 * Liste de toutes les proies qui ne sont plus consommable
//	 */
//	ArrayList<Proie> nonConsommables = new ArrayList<Proie>();
//	
//	/**
//	 * Liste des fourmis mortes
//	 * 
//	 * --> Ajouter un getter
//	 * 
//	 * Une fourmi qui meurt doit se retrouver directement dans la liste des fourmis mortes
//	 */
//	ArrayList<EtreVivant> mortes = new ArrayList<EtreVivant>();
//	
//	/**
//	 * Dans fourmiliere
//	 */
//	public void stepFourmiliere() {
////		...
//		
//		for(Proie consommable : consommables) {
//			consommable.setDateDebutConsommation(consommable.getDateDebutConsommation() + 1);
//			if(consommable.getDateDebutConsommation() + 40 == Simulateur.getHeures())
//				this.consommables.remove(consommable);
//				this.nonConsommables.add(consommable);
//		}
//		
////		...
//	}
//	
//	/**
//	 * Dans Fourmi
//	 */
//	public void stepFourmi(Fourmiliere fourmiliere) {
////		...
//		
//		// On donne le poids de la fourmi actuelle
//		if(EtatEnum.LARVE == this.getEtat())
//			this.manger(fourmiliere, this.getPoids());
//		if(EtatEnum.ADULTE == this.getEtat()) {
//			this.manger(fourmiliere, (this.getPoids() / 3));
//			this.nettoyer(fourmiliere);
//		}
//		
////		...
//	}
//	
//	/**
//	 * Dans Fourmi
//	 */
//	private void manger(Fourmiliere fourmiliere, double quantiteNecessaire) {
////		La fourmi prend la premi�re proie de la liste --> Ou, si on pr�f�re, la proie qui est la depuis + longtemps
//		Proie consommable = fourmiliere.getConsommables().get(0);
////		La fourmi mange le tier de son poids
//		if(quantiteNecessaire > consommable.getPoids()) {
//			quantiteNecessaire -= consommable.getPoids();
//			consommable.setPoids(0.0);
//			fourmiliere.getConsommables().remove(consommable);
//			// Recursivite jusqu'� plus fin
//			if (quantiteNecessaire > 0.0)
//				this.manger(fourmiliere, quantiteNecessaire);
//		} else
//			consommable.setPoids(consommable.getPoids() - quantiteNecessaire);
//	}
//	
//	/**
//	 * Dans fourmi
//	 */
//	private void nettoyer(Fourmiliere fourmiliere) {
//		// Calcul du ratio de nb fourmi morte et non-consommables pour voir le plus urgent
//		double ratioMortes = fourmiliere.getMortes().size() / fourmiliere.getPopulation();					// Ne doit pas exceder 0.3
//		double ratioNonConsommables = fourmiliere.getNonConsommables() / fourmiliere.getConsommables();		// Ne doit pas exc�der 1
//		
//		final double MAX_RATIO_MORTES = 0.3;
//		final double MAX_RATIO_NON_CONSOMMABLES = 1.0;
//		
//		// Choix du ratio a appliquer
//		double diffMortes = MAX_RATIO_MORTES - ratioMortes;
//		double diffNonConsommables = MAX_RATIO_NON_CONSOMMABLES - ratioNonConsommables;
//		
//		if(Math.min(diffMortes, diffNonConsommables) == diffMortes) {
//			// On enleve des mortes
//		} else {
//			// On enleve des non-consommables
//		}
//	}
//	
//	/**
//	 * Main Inutile
//	 * @param args
//	 */
//	public static void main(String[] args) {
//	}
//
//}
