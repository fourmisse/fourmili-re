package tests;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import model.Coordonnee;
import model.Terrain;

class TerrainTest {

	@Test
	void testConstructeur() {
		Terrain terrain = new Terrain();
		assertEquals(0, terrain.getListeFourmiliere().size());
		assertEquals(0, terrain.getListePheromone().size());
		assertEquals(0, terrain.getListeProies().size());
		
	}

	@Test
	void testAddPheromone() {
		Terrain terrain = new Terrain();
		assertEquals(0, terrain.getListePheromone().size());
		terrain.addPheromoneOuvriere(1, 2);
		assertEquals(1, terrain.getListePheromone().size());
		Coordonnee coordonnee = new Coordonnee(1, 2);
		assertTrue(terrain.getListePheromone().containsKey(coordonnee));
		coordonnee = new Coordonnee(2, 1);
		assertFalse(terrain.getListePheromone().containsKey(coordonnee));
		
	}
	
	
}
