package tests;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import model.Fourmi;
import model.Fourmiliere;
import model.Proie;
import model.Terrain;
import model.etat.Adulte;
import model.role.Ouvriere;

class OuvriereTest {

	
	@Test
	void testAttaqueProie(){
		Fourmi fourmi = new Fourmi(0, 0);
		Fourmiliere fourmiliere = new Fourmiliere(new Fourmi(20, 20));
		Terrain terrain = new Terrain();

		Adulte adulte = new Adulte(fourmi, fourmiliere, terrain);
		while(!(adulte.getRole() instanceof Ouvriere)) {
			adulte = new Adulte(fourmi, fourmiliere, terrain);
		}
		
		
		Proie proie = new Proie(terrain);
		proie.setX(0);
		proie.setY(0);
		
		adulte.getRole().step(fourmi, fourmiliere, terrain);
		
		Ouvriere ouvriere = (Ouvriere) adulte.getRole();
		

		
		
	}
}
