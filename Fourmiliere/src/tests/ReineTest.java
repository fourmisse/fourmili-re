package tests;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import model.Fourmi;
import model.Fourmiliere;
import model.Terrain;
import model.etat.Adulte;
import model.role.Reine;

class ReineTest {

	@Test
	void testReine() {

		Terrain terrain = new Terrain();
		Fourmi reine = new Fourmi(0, 0);
		Adulte adulte = new Adulte(reine, null, terrain);

		assertNotNull(((Reine) adulte.getRole()).creerFourmiliere(reine, terrain));

		System.out.println(reine.getEtat().getDuree());

		assertTrue(reine.getEtat().getDuree() >= 4 * 365 * 24);
		assertTrue(reine.getEtat().getDuree() < 10 * 365 * 24);

	}

}
