package tests;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import model.Fourmi;
import model.Fourmiliere;
import model.etat.Larve;
import model.etat.Oeuf;

class LarveTest {

	@Test
	void testNext() {
		Fourmiliere fourmiliere = new Fourmiliere(new Fourmi(0,0));
		Fourmi fourmi = new Fourmi(0, 0);
		Larve larve = new Larve(fourmi);
		fourmiliere.getLarvesFourmis().add(fourmi);
		assertEquals(1, fourmiliere.getLarvesFourmis().size());
		
		assertTrue(fourmi.getEtat() instanceof Larve);
		assertEquals(larve, fourmi.getEtat());

		larve.next(fourmi, fourmiliere, null);
		
		assertEquals(1, fourmiliere.getNymphesFourmis().size());
		assertEquals(0, fourmiliere.getLarvesFourmis().size());
		
		
	}
}
