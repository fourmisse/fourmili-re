package tests;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import model.Fourmi;
import model.Fourmiliere;
import model.Terrain;
import model.etat.Cadavre;
import model.etat.Oeuf;

class CadavreTest {

	@Test
	void testNext() {
		Terrain terrain = new Terrain();
		Fourmiliere fourmiliere = new Fourmiliere(new Fourmi(0,0));
		Fourmi fourmi = new Fourmi(0, 0);
		Cadavre cadavre = new Cadavre(fourmi);
		fourmiliere.getCadavresFourmis().add(fourmi);
		
		assertEquals(cadavre, fourmi.getEtat());
		assertEquals(1, fourmiliere.getCadavresFourmis().size());
		assertNull(cadavre.next(fourmi, fourmiliere, terrain));
		
	}

}
