package tests;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import model.Fourmi;
import model.Fourmiliere;
import model.Terrain;
import model.etat.Oeuf;

class OeufTest {

	@Test
	void testNext() {
		Fourmiliere fourmiliere = new Fourmiliere(new Fourmi(0,0));
		Fourmi fourmi = new Fourmi(0, 0);
		Oeuf oeuf = new Oeuf(fourmi);
		fourmiliere.getOeufsFourmis().add(fourmi);
		assertEquals(1, fourmiliere.getOeufsFourmis().size());
		
		assertTrue(fourmi.getEtat() instanceof Oeuf);
		assertEquals(oeuf, fourmi.getEtat());

		oeuf.next(fourmi, fourmiliere, null);
		
		assertEquals(1, fourmiliere.getLarvesFourmis().size());
		assertEquals(0, fourmiliere.getOeufsFourmis().size());
		
		
	}

}
