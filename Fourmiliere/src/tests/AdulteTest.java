package tests;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import model.Fourmi;
import model.Fourmiliere;
import model.Terrain;
import model.etat.Adulte;
import model.role.RoleEnum;

class AdulteTest {

	@Test
	void testConstructeur() {
		Terrain terrain = new Terrain();
		Fourmiliere fourmiliere = new Fourmiliere(new Fourmi(0,0));
		Fourmi fourmi = new Fourmi(0,0);
		fourmiliere.getAdultesFourmis().add(fourmi);
		Adulte adulte = new Adulte(fourmi, fourmiliere, terrain);
		assertNotNull(adulte.getRole());
		
		for(int i = 0; i < 10000; i ++) {
			adulte = new Adulte(fourmi, fourmiliere, terrain);			
			assertTrue(adulte.getDuree() >= 1.0*365*24);
			assertTrue(adulte.getDuree() < 2.5*365*24);
		}

	}
	
	@Test
	void testNext() {
		Terrain terrain = new Terrain();
		Fourmiliere fourmiliere = new Fourmiliere(new Fourmi(0,0));
		Fourmi fourmi = new Fourmi(0, 0);
		Adulte adulte = new Adulte(fourmi, fourmiliere, terrain);
		fourmiliere.getAdultesFourmis().add(fourmi);
		assertEquals(1, fourmiliere.getAdultesFourmis().size());
		
		assertTrue(fourmi.getEtat() instanceof Adulte);
		assertEquals(adulte, fourmi.getEtat());

		adulte.next(fourmi, fourmiliere, null);
		
		assertEquals(1, fourmiliere.getCadavresFourmis().size());
		assertEquals(0, fourmiliere.getAdultesFourmis().size());
		
	}
	
	@Test
	void testRatio() {
		Terrain terrain = new Terrain();
		Fourmiliere fourmiliere = new Fourmiliere(new Fourmi(0,0));
		for(int i = 0; i < 100000; i++) {
			Fourmi fourmi = new Fourmi(0, 0);
			fourmiliere.getAdultesFourmis().add(fourmi);
			Adulte adulte = new Adulte(fourmi, fourmiliere, terrain);			
		}
		
		assertTrue(fourmiliere.getOuvrieresVivantes().size()/(double)fourmiliere.getNbAdultes() > RoleEnum.OUVRIERE.getRatioMin());
		assertTrue(fourmiliere.getOuvrieresVivantes().size()/(double)fourmiliere.getNbAdultes() < RoleEnum.OUVRIERE.getRatioMax());
		
		assertTrue(fourmiliere.getSoldatsVivantes().size()/(double)fourmiliere.getNbAdultes() > RoleEnum.SOLDAT.getRatioMin());
		assertTrue(fourmiliere.getSoldatsVivantes().size()/(double)fourmiliere.getNbAdultes() < RoleEnum.SOLDAT.getRatioMax());
		
		assertTrue(fourmiliere.getSexuesVivantes().size()/(double)fourmiliere.getNbAdultes() > RoleEnum.SEXUE.getRatioMin());
		assertTrue(fourmiliere.getSexuesVivantes().size()/(double)fourmiliere.getNbAdultes() < RoleEnum.SEXUE.getRatioMax());
		
		
		
	}

}
