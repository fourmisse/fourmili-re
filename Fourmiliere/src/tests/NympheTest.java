package tests;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import model.Fourmi;
import model.Fourmiliere;
import model.etat.Nymphe;
import model.etat.Oeuf;

class NympheTest {

	@Test
	void testNext() {
		Fourmiliere fourmiliere = new Fourmiliere(new Fourmi(0,0));
		Fourmi fourmi = new Fourmi(0, 0);
		Nymphe nymphe = new Nymphe(fourmi);
		fourmiliere.getNymphesFourmis().add(fourmi);
		assertEquals(1, fourmiliere.getNymphesFourmis().size());
		
		assertTrue(fourmi.getEtat() instanceof Nymphe);
		assertEquals(nymphe, fourmi.getEtat());

		nymphe.next(fourmi, fourmiliere, null);
		
		assertEquals(1, fourmiliere.getAdultesFourmis().size());
		assertEquals(0, fourmiliere.getNymphesFourmis().size());
		
		
	}
}
