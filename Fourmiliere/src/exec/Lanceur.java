package exec;

import model.Fourmi;
import model.Fourmiliere;
import model.Simulateur;
import model.etat.Adulte;
import view.Carte;

/**
 * Permet le lancement de la simulation
 * 
 * @author Gurvan & Samuel
 *
 */
public class Lanceur {

	/**
	 * 
	 * @param args
	 * @throws Exception
	 */
	public static void main(String[] args) throws Exception {

		Carte carte = new Carte();

		Simulateur simulateur = carte.getSimulateur();

		// Notre reine...
		Fourmi fourmiReine = new Fourmi(45, 45);
		// ...est adulte
		new Adulte(fourmiReine, null, simulateur.getTerrain());

		Fourmiliere fourmiliere = simulateur.getTerrain().getListeFourmiliere().get(0);
		for (int i = 0; i < 28000; i++) {
			if (i > 400)
//				Thread.sleep(Simulateur.NB_MS_HEURE);
				simulateur.step();
		}
	}
}
