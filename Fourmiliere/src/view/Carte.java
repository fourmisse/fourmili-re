package view;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Point;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import graphicLayer.GOval;
import graphicLayer.GRect;
import graphicLayer.GSpace;
import graphicLayer.GString;
import model.Coordonnee;
import model.Fourmi;
import model.Fourmiliere;
import model.Pheromone;
import model.Proie;
import model.Simulateur;
import model.Terrain;

/**
 * Vue de la simulation. On peut observer l'�volution des fourmis et des proies.
 * 
 * @author Gurvan & Samuel
 *
 */
@SuppressWarnings("serial")
public class Carte extends GSpace implements PropertyChangeListener {

	/**
	 * Nombre de pixel pour un bloc de la carte
	 */
	private static final int TAILLE_BLOCS = 6;

	/**
	 * Map contenant les fourmili�res l'�l�ment graphique qui est accosi� � chacune
	 */
	private Map<Fourmiliere, GRect> fourmilieres;

	/**
	 * Map contenant les ph�romones l'�l�ment graphique qui est accosi� � chacune
	 */
	private Map<Coordonnee, GRect> pheromones;

	/**
	 * Map contenant les fourmis l'�l�ment graphique qui est accosi� � chacune
	 */
	private Map<Fourmi, GOval> fourmis;

	/**
	 * Map contenant les proies l'�l�ment graphique qui est accosi� � chacune
	 */
	private Map<Proie, GOval> proies;

	/**
	 * Simulateur de l'application
	 */
	private Simulateur simulateur;

	/**
	 * Longueur de la fen�tre de l'application
	 */
	public final static int LONGUEUR = 800;

	/**
	 * Largeur de la fen�tre de l'application
	 */
	public final static int LARGEUR = 600;

	/**
	 * �l�ment principale de la fen�tre contenant l'affichage de la simulation et
	 * des statistiques
	 */
	private GRect main;

	/**
	 * �l�ment graphique qui affiche la simulation et qui se met � jour � chaque
	 * �tape donn�e par le simulateur
	 */
	private GRect simulation;

	/**
	 * �l�ment graphique qui affiche les statistiques de la simulation et qui se met
	 * � jour � chaque �tape donn�e par le simulateur
	 */
	private GRect informations;

	/**
	 * Affichage des heures
	 */
	private GString heures;

	/**
	 * Affichage des saisons
	 */
	private GString saison;

	/**
	 * Affichage du nombre total d'oeufs
	 */
	private GString oeufs;

	/**
	 * Affichage du nombre total de larves
	 */
	private GString larves;

	/**
	 * Affichage du nombre total de nymphes
	 */
	private GString nymphes;

	/**
	 * Affichage du nombre total d'adultes
	 */
	private GString adultes;

	/**
	 * Affichage du nombre total de cadavres
	 */
	private GString cadavres;

	/**
	 * Affichage du nombre total de proies � manger dans la fourmili�re
	 */
	private GString proiesAManger;

	/**
	 * Constructeur de la carte qui initialise les �l�ments
	 * 
	 * @throws Exception
	 */
	public Carte() throws Exception {
		super("Les fourmisses", new Dimension(LONGUEUR, LARGEUR));
		this.simulateur = new Simulateur();
		this.simulateur.addListener(this);
		this.fourmilieres = new HashMap<Fourmiliere, GRect>();
		this.pheromones = new HashMap<Coordonnee, GRect>();
		this.fourmis = new HashMap<Fourmi, GOval>();
		this.proies = new HashMap<Proie, GOval>();
		this.initContent();
		this.updateContent();
		this.open();
	}

	/**
	 * Permet la mise en place tous les �l�ments graphiques
	 * 
	 * @throws Exception
	 */
	private void initContent() throws Exception {
		this.main = new GRect();
		this.informations = new GRect();
		this.simulation = new GRect();

		this.main.setDimension(new Dimension(LONGUEUR, LARGEUR));
		this.simulation.setDimension(new Dimension(LONGUEUR - 200, LARGEUR));
		this.informations.setDimension(new Dimension(200, LARGEUR));

		this.informations.setPosition(new Point(LONGUEUR - 200, 0));

		this.main.setColor(Color.WHITE);
		this.simulation.setColor(Color.DARK_GRAY);
		this.informations.setColor(Color.ORANGE);

		this.main.addSubElement(this.simulation);
		this.main.addSubElement(this.informations);

		this.heures = new GString("Heures : " + Simulateur.getHeures());
		this.saison = new GString("Saison : " + this.simulateur.getSaison());
		this.oeufs = new GString("Oeufs : 0");
		this.larves = new GString("Larves : 0");
		this.nymphes = new GString("Nymphes : 0");
		this.adultes = new GString("Adultes : 0");
		this.cadavres = new GString("Cadavres : 0");
		this.proiesAManger = new GString("Proies : 0");

		this.informations.addSubElement(heures);
		this.informations.addSubElement(saison);
		this.informations.addSubElement(oeufs);
		this.informations.addSubElement(larves);
		this.informations.addSubElement(nymphes);
		this.informations.addSubElement(adultes);
		this.informations.addSubElement(cadavres);
		this.informations.addSubElement(proiesAManger);

		this.heures.setPosition(new Point(10, 10));
		this.saison.setPosition(new Point(10, 30));
		this.oeufs.setPosition(new Point(10, 50));
		this.larves.setPosition(new Point(10, 70));
		this.nymphes.setPosition(new Point(10, 90));
		this.adultes.setPosition(new Point(10, 110));
		this.cadavres.setPosition(new Point(10, 130));
		this.proiesAManger.setPosition(new Point(10, 150));

		this.addElement(this.main);
	}

	@Override
	public void propertyChange(PropertyChangeEvent evt) {
		try {
			this.updateContent();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * R�cup�ration du simulateur
	 * 
	 * @return Simulateur utilis�
	 */
	public Simulateur getSimulateur() {
		return this.simulateur;
	}

	/**
	 * Mise � jour des contenus graphiques
	 * 
	 * @throws Exception
	 */
	public void updateContent() throws Exception {
		this.updateContentSimulation();
		this.updateContentInfo();
	}

	/**
	 * Mise � jour des statistiques affich�es
	 * 
	 * @throws Exception
	 */
	private void updateContentInfo() throws Exception {
		this.heures.setString(String.valueOf(Simulateur.getHeures()));
		this.saison.setString(this.simulateur.getSaison().toString());

		int nbOeufs = 0;
		int nbLarves = 0;
		int nbNymphes = 0;
		int nbAdultes = 0;
		int nbCadavres = 0;
		int nbProiesAManger = 0;
		double poidsAManger = 0;

		for (Fourmiliere f : simulateur.getTerrain().getListeFourmiliere()) {
			nbOeufs += f.getNbOeufs();
			nbLarves += f.getNbLarves();
			nbNymphes += f.getNbNymphes();
			nbAdultes += f.getNbAdultes();
			nbCadavres += f.getNbCadavres();
			nbProiesAManger += f.getProieAManger().size();
			for (Proie proie : f.getProieAManger()) {
				poidsAManger += proie.getPoids();
			}
		}

		this.oeufs.setString("Oeufs : " + nbOeufs);
		this.larves.setString("Larves : " + nbLarves);
		this.nymphes.setString("Nymphes : " + nbNymphes);
		this.adultes.setString("Adultes : " + nbAdultes);
		this.cadavres.setString("Cadavres : " + nbCadavres);
		this.proiesAManger.setString("Proies : " + nbProiesAManger + "(" + String.format("%.2f", poidsAManger) + "mg)");
	}

	/**
	 * Mise � jour de l'affichage du simulateur
	 */
	private void updateContentSimulation() {
		Terrain terrain = this.simulateur.getTerrain();

		// Sur les ph�romones
		this.updatePheromoneSimulation();

		// Sur les fourmilieres
		for (Fourmiliere fourmiliere : terrain.getListeFourmiliere()) {
			this.addOrRemoveFourmiliereMap(fourmiliere);

			// Sur les fourmis
			for (Fourmi fourmi : fourmiliere.getFourmisVivante()) {
				this.addFourmiMap(fourmi);
			}

			// Sur les cadavres
			for (Fourmi cadavre : fourmiliere.getCadavresFourmis()) {
				this.removeFourmiMap(cadavre);
			}

			// Sur les proies
			for (Proie proie : fourmiliere.getProieAManger()) {
				this.removeProieMap(proie);
			}

		}

		// Ajout des proies
		for (Proie proie : this.simulateur.getTerrain().getListeProies()) {
			this.addProieMap(proie);
		}

	}

	/**
	 * Permet de retirer une proie de la carte
	 * 
	 * @param proie
	 */
	private void removeProieMap(Proie proie) {
		if (this.proies.containsKey(proie) && proie.isVaincue()) {
			GOval proieOval = this.proies.get(proie);
			proieOval.setColor(Color.GREEN);
			this.simulation.removeSubElement(proieOval);
			this.proies.remove(proie);
		}
	}

	/**
	 * Permet l'ajout et la suppression des ph�romones de la carte
	 */
	private void updatePheromoneSimulation() {
		Map<Coordonnee, Pheromone> pheromones = this.simulateur.getTerrain().getListePheromone();
		Iterator<Coordonnee> ite = pheromones.keySet().iterator();

		// On ajoute � la carte toutes les ph�romones pos�es
		while (ite.hasNext()) {
			Coordonnee coordonnee = ite.next();

			if (!this.pheromones.containsKey(coordonnee)) {
				GRect pheromoneRect = new GRect();
				this.simulation.addSubElement(pheromoneRect);
				pheromoneRect.setDimension(new Dimension(TAILLE_BLOCS, TAILLE_BLOCS));
				pheromoneRect.setColor(new Color(255, 255, 255, 50));
				pheromoneRect
						.setPosition(new Point(coordonnee.getX() * TAILLE_BLOCS, coordonnee.getY() * TAILLE_BLOCS));
				this.pheromones.put(coordonnee, pheromoneRect);
			}

		}

		// On retire toutes les ph�romones qui ne sont plus valides
		List<Coordonnee> toRemove = this.simulateur.getTerrain().verifierPheromone();
		for (Coordonnee coordonnee : toRemove) {
			this.simulation.removeSubElement(this.pheromones.get(coordonnee));
			this.pheromones.remove(coordonnee);
		}

	}

	/**
	 * Permet de retirer les fourmis de la carte
	 * 
	 * @param fourmi
	 */
	private void removeFourmiMap(Fourmi fourmi) {
		if (this.fourmis.containsKey(fourmi)) {
			GOval fourmiOval = this.fourmis.get(fourmi);
			fourmiOval.setColor(Color.GREEN);
			this.simulation.removeSubElement(fourmiOval);
			this.fourmis.remove(fourmi);
		}
	}

	/**
	 * Permet l'ajout de fourmis � la carte
	 * 
	 * @param fourmi
	 */
	private void addFourmiMap(Fourmi fourmi) {
		GOval fourmiOval;
		if (!this.fourmis.containsKey(fourmi)) {
			fourmiOval = new GOval();
			fourmiOval.setColor(Color.PINK);
			fourmiOval.setDimension(new Dimension(TAILLE_BLOCS, TAILLE_BLOCS));
			this.simulation.addSubElement(fourmiOval);
			this.fourmis.put(fourmi, fourmiOval);
		} else {
			fourmiOval = this.fourmis.get(fourmi);
		}

		fourmiOval.setPosition(new Point(fourmi.getX() * TAILLE_BLOCS, fourmi.getY() * TAILLE_BLOCS));
	}

	/**
	 * Permet l'ajout de proie � la carte
	 * 
	 * @param proie
	 */
	private void addProieMap(Proie proie) {
		GOval proieOval;
		if (!this.proies.keySet().contains(proie)) {
			proieOval = new GOval();
			proieOval.setColor(Color.MAGENTA);
			proieOval.setDimension(new Dimension(TAILLE_BLOCS, TAILLE_BLOCS));
			this.simulation.addSubElement(proieOval);
			this.proies.put(proie, proieOval);
		} else {
			proieOval = this.proies.get(proie);
		}

		proieOval.setPosition(new Point(proie.getX() * TAILLE_BLOCS, proie.getY() * TAILLE_BLOCS));
	}

	/**
	 * Permet d'ajouter ou retirer une fourmili�re � la carte
	 * 
	 * @param fourmiliere
	 */
	private void addOrRemoveFourmiliereMap(Fourmiliere fourmiliere) {
		if (!this.fourmilieres.keySet().contains(fourmiliere)) {

			GRect fourmiliereRect = new GRect();
			this.simulation.addSubElement(fourmiliereRect);

			fourmiliereRect.setDimension(new Dimension(TAILLE_BLOCS, TAILLE_BLOCS));
			fourmiliereRect
					.setPosition(new Point(fourmiliere.getX() * TAILLE_BLOCS, fourmiliere.getY() * TAILLE_BLOCS));
			fourmiliereRect.setColor(Color.RED);
			this.fourmilieres.put(fourmiliere, fourmiliereRect);
		}
	}
}